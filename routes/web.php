<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/halopln', function() {
    return 'Halo PLN';
});

Route::get('/halo/{nama?}', function($nama="Budi") {
    return 'Halo '.$nama;
});

Route::get('/halaman', function() {
    return view('halaman');
});