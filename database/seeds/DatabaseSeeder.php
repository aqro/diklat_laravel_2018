<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BukuTableSeeder::class);
        $this->call(BukuGenreSeeder::class);
        $this->call(BackpackSeeder::class);
    }
}
