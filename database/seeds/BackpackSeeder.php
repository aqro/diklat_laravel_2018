<?php

use Illuminate\Database\Seeder;

class BackpackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert(
            [
                'name' => 'Resa Aqrobby',
                'email' => 'resa.aqrobby@pln.co.id',
                'password'  => '$2y$10$XVCkhHkKODKLXaAgizykOu0UBv2VSYH0xlFibGX0EJCzXcnDCfv4a'
            ]
        );
    }
}
