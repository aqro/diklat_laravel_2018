<?php

use Illuminate\Database\Seeder;

class BukuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('buku')->insert([
        [
            'judul' => 'How to learn Laravel',
            'author' => 'Morgan Freeman',
            'genre_id'  => '1',
            'published_at'  => '2018-01-01',
        ],
        [
            'judul' => 'How to learn NodeJS',
            'author' => 'JP Morgan',
            'genre_id'  => '2',
            'published_at'  => '2018-01-01',
        ],
        [
            'judul' => 'How to learn React',
            'author' => 'Mark Zulkerberg',
            'genre_id'  => '3',
            'published_at'  => '2018-01-01',
            
        ],
        [
            'judul' => 'How to learn VueJS',
            'author' => 'Mark Twain',
            'genre_id'  => '4',
            'published_at'  => '2018-01-01',
        ]
        ]);
    }
}
